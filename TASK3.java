package com.example;

import java.util.ArrayList;
import java.util.List;


/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {
    // Função para receber um inteiro que determina o tamanho da String, retornando uma String aleatória
    public String getRandomStr(int strSize) {
        String randomStr = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder sb = new StringBuilder(strSize);
        
        for (int i = 0; i < strSize; i++) {
            int pos = (int)(randomStr.length() * Math.random());
            sb.append(randomStr.charAt(pos));
        }
        return sb.toString();
    }
    
    // Função que retorna uma lista, recebendo dois parâmetros, um sendo o tamanho da lista e o outro o tamanho da String que será gerada ao chamar a função
    //getRandomStr(int strSize)
    public List<String> generateList(int listSize, int strSize){
        List<String> lista = new ArrayList<String>();
        String generatedStr = null;
        for (int i = 0; i < listSize; i++){
            generatedStr = getRandomStr(strSize);
            lista.add(generatedStr);
        }
        return lista;
    }
    
    // Por fim uma função que retorna a quantidade de elementos distintos na lista de Strings
    public int uniqueElements (List<String> lista) {
        return (int) lista.stream().distinct().count();
    }
}
