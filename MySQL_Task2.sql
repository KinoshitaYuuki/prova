SELECT COUNT(DISTINCT emp_no) AS Quantidade, gender AS Sexo, birth_date AS Data_de_nascimento, hire_date AS Data_de_contratação
FROM employees
GROUP BY gender, birth_date, hire_date;