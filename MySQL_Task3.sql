SELECT gender AS Sexo, AVG(salaries.salary) AS Média, MIN(salaries.salary) AS Mínimo, MAX(salaries.salary) AS Máximo
FROM employees
INNER JOIN salaries
ON employees.emp_no = salaries.emp_no
GROUP BY gender;