package com.example;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {
    // Strings para atribuir a palavra original e a palavra reversa
    public String word;
    public String backwardWord;
    
    // Função que retorna um boolean para verificar se a palavra é ou não um palíndromo, recebendo como parâmetro uma única String
    public boolean isPalindrome (String word) {
        try {
            // Buffer de String para armazenar a String e realizar a função reverse(), com adição da toString()
            //para a variável backwardWord conseguir receber uma String
            StringBuilder sb = new StringBuilder(word);
            backwardWord = sb.reverse().toString();
            if (backwardWord.equals(word)){
                return true;
            };
        } catch (NullPointerException ex){
            System.out.println("String vazia");
        }finally {        
            return false;
        }
    }
}
