package com.example;

import java.util.ArrayList;
import java.util.List;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */
public class TASK2 { 
    public List<String> adicionarItems (List<String> lista) {
        // Adicionando 9 cores na lista
        String a = "Amarelo";
        String b = "Azul";
        String c = "Preto";
        String d = "Branco";
        String e = "Lilas";
        String f = "Laranja";
        String g = "Cinza";
        String h = "Verde";
        String i = "Rosa";
        // nove elementos
        lista.add(a);
        lista.add(b);
        lista.add(c);
        lista.add(d);
        lista.add(e);
        lista.add(f);
        lista.add(g);
        lista.add(h);
        lista.add(i);
        return lista;
    }  
    
    public void printarLista (List<String> lista){
        System.out.println(lista);
    }
    
    public void removerMeio (List<String> lista) {
        int meio; // metade seria 4.5, mas listas e vetores iniciam no 0, nesse caso seria a posição 4. -> 0 1 2 3 [4] 5 6 7 8 <- meio da lista
        meio = lista.size()/2;
        lista.remove(meio);
        printarLista(lista);
    }
}